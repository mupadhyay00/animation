import React, { Component } from 'react';
import { View, Text } from 'react-native';

class TinderCard extends Component {
  renderAll() {
    return (
      <ul>
        this.props.data.map(item => {
          <li>this.props.renderCard(item)</li>
        });
      </ul>
    ); 
  }
  render() {
    return (
      <View>
        {this.renderAll()}
      </View>
    );
  }
}

export default TinderCard;
