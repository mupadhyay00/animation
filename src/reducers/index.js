import { combineReducers } from 'redux';
import BallAnimation from './BallAnimationReducer.js';

export default combineReducers({
  BallAnimation: BallAnimation
});
