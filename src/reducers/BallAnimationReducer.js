const INITIAL_STATE = {
  ball: {}
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'PROFILE_PIC_UPDATE': {
      return { ...state };
    }
    default:
      return state;
  }
};
