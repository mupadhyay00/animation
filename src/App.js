import React, { Component } from 'react';
import { Provider } from 'react-redux';

import store from './store';
// import Ball from './screens/Ball.js';
import Login from './screens/Login.js';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Login />
      </Provider>
    );
  }
}

export default App;
