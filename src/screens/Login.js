import React, { Component } from 'react';
import { View, Text, Image, TextInput, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

const myIcon = (<Icon name="rocket" size={30} color="#900" />);
class Login extends Component {
  render() {
    return (
      <View style={styles.fullContainer}>
      <Image
      resizeMode="cover"
      source={require('../assets/images/back.png')}
      style={styles.backgroundImg}
      >
        <View style={styles.formAtCenter}>
          <Image
            source={require('../assets/images/spin.png')}
          />
        </View>


        <View style={styles.formAtCenter}>
        <View
        style={styles.inputFieldContainer}
        ><Icon name="user" size={20} color="#fff" style={styles.iconFontSize} />
        <TextInput
        underlineColorAndroid='transparent'
        style={styles.inputField}
        placeholder={'Email'}
        placeholderTextColor={'#FFF'}
        />
        </View>
          <View
          style={styles.inputFieldContainer}
          ><Icon name="lock" size={20} color="#fff" style={styles.iconFontSize} />
          <TextInput
          underlineColorAndroid='transparent'
          placeholder={'PASSWord'}
          style={styles.inputField}
          placeholderTextColor={'#FFF'}
          />
          </View>
          <View
            style={[styles.socialButton,
                styles.googleColor,
                styles.margins]}
          >
          <TouchableOpacity>
            <Text style={styles.RedBotton}>
             SIGN IN
            </Text>
          </TouchableOpacity>
                  </View>
        </View>
      </Image>

      </View>
    );
  }
}

const styles = {
  fullContainer: {
    flex: 1
  },
  inputField: {
    borderWidth: 0,
    color: 'white',
    flex: 6
  },
  backgroundImg: {
    flex: 1,
  width: null,
  height: null
  },
  RedBotton: {
    paddingLeft: 110,
    paddingTop: 20,
    paddingRight: 110,
    paddingBottom: 20,
    marginTop: 30,
        marginBottom: 10,
        borderRadius: 60,
        alignItems: 'center',
        backgroundColor: '#FF3366'
  },
  iconFontSize: {
    fontWeight: 'lighter',
    flex: 1
  },
  textColor: {
    color: '#ffffff'
  },
  inputFieldContainer: {
    height: 50,
    width: 300,
        backgroundColor: 'transparent',
        borderColor: 'white',
        borderBottomWidth: 1,
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center'
  },
  formAtCenter: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }

};

export default Login;
