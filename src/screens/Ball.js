import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { View, Text } from 'react-native';
import Circle from '../components/Circle';

class Ball extends Component {
  render() {
    return(
      <View style={styles.fullContainer}>
      <Circle />
      </View>
    );
  }
}

const styles = {
  fullContainer: {
    flex:1
  }
}

export default Ball;
